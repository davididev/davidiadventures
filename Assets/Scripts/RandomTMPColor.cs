﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTMPColor : MonoBehaviour {

    public TMPro.TextMeshProUGUI textUIObject;
    public Color[] possibleColors;

    public float minTimer = 1f, maxTimer = 1f;

	// Use this for initialization
	void Start () {
        StartCoroutine(GlowRoutine());
	}

    Color GetRandomColor()
    {
        int x = Random.Range(0, possibleColors.Length - 1);
        return possibleColors[x];
    }

    IEnumerator GlowRoutine()
    {
        vg = textUIObject.colorGradient;
        loadedVG = true;
        while (gameObject)
        {
            
            float thisStep = Random.Range(minTimer, maxTimer);
            iTween.ValueTo(gameObject, iTween.Hash("from", vg.topLeft, "onupdate", "SetColorCorner1", "to", GetRandomColor(), "time", thisStep));
            iTween.ValueTo(gameObject, iTween.Hash("from", vg.topRight, "onupdate", "SetColorCorner2", "to", GetRandomColor(), "time", thisStep));
            iTween.ValueTo(gameObject, iTween.Hash("from", vg.bottomLeft, "onupdate", "SetColorCorner3", "to", GetRandomColor(), "time", thisStep));
            iTween.ValueTo(gameObject, iTween.Hash("from", vg.bottomRight, "onupdate", "SetColorCorner4", "to", GetRandomColor(), "time", thisStep));
            yield return new WaitForSeconds(thisStep);
        }
    }
    TMPro.VertexGradient vg;
    bool loadedVG = false;
    void SetColorCorner1(Color c)
    {
        vg.topLeft = c;
    }

    void SetColorCorner2(Color c)
    {
        vg.topRight = c;
    }
    void SetColorCorner3(Color c)
    {
        vg.bottomLeft = c;
    }
    void SetColorCorner4(Color c)
    {
        vg.bottomRight = c;
    }

    // Update is called once per frame
    void Update () {
	    if(loadedVG == true)
        {
            textUIObject.colorGradient = vg;
        }
	}
}
