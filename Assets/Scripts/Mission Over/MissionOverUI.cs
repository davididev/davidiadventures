﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MissionOverUI : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI text;
    [SerializeField] private GameObject endButton;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(LOLIHAZCHEEZBURGER());
    }

    IEnumerator LOLIHAZCHEEZBURGER()
    {
        AudioSource lol = Camera.main.gameObject.AddComponent<AudioSource>();
        lol.clip = Resources.Load<AudioClip>("Audio/TextScroll");
        lol.loop = true;
        

        string[] s = StoryDatabase.instance.GetEnding().Split('*');
        for (int x = 0; x < s.Length; x++)
        {
            lol.Play();
            string line = s[x];
            for (int i = 0; i < s[x].Length; i++)
            {
                text.text = text.text + line[i];
                yield return new WaitForSeconds(0.05f);
            }
            lol.Stop();
            yield return new WaitForSeconds(1f);
            text.text = text.text + "\n\n";
        }

        lol.Stop();

        endButton.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
