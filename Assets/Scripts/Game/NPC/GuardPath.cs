﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardPath : MonoBehaviour
{
    [SerializeField] float horizontalDistance, verticalDistance;
    private Vector3 startingPos;
    private float leftLimit, rightLimit, upLimit, downLimit;

    private CharController cc;
    // Start is called before the first frame update
    void Start()
    {
        startingPos = transform.position;
        leftLimit = startingPos.x - (horizontalDistance / 2f);
        rightLimit = startingPos.x + (horizontalDistance / 2f);
        upLimit = startingPos.y + (verticalDistance / 2f);
        downLimit = startingPos.y - (verticalDistance / 2f);
    }

    private Transform player;
    // Update is called once per frame
    void Update()
    {
        if (player == null)
            player = GameObject.FindGameObjectWithTag("Player").transform;

        if (cc == null)
            cc = GetComponent<CharController>();

        Vector3 targetPos = player.position;
        if (targetPos.x < leftLimit)
            targetPos.x = leftLimit;
        if (targetPos.x > rightLimit)
            targetPos.x = rightLimit;
        if (targetPos.y > upLimit)
            targetPos.y = upLimit;
        if (targetPos.y < downLimit)
            targetPos.y = downLimit;

        cc.targetPosition = new Vector2(targetPos.x, targetPos.y);
    }

    //Can be called by dialogue system
    public void TurnOffPath()
    {
        this.enabled = false;
        cc.targetPosition = Vector2.zero;
    }
}
