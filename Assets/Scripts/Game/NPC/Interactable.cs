﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    [SerializeField] TextAsset onTalkDialogue, onLookAtDialogue, onTouchDialogue, onItemDialogue;

    private GameObject indicatorInstance;

    public static Interactable selectedInteractable;

    // Start is called before the first frame update
    void Start()
    {
        GameObject g = Resources.Load<GameObject>("Prefab/InteractingZone") as GameObject;
        indicatorInstance = GameObject.Instantiate(g, transform);
        indicatorInstance.transform.position = transform.position + (Vector3.down * 0.25f);
        indicatorInstance.SetActive(false);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.tag == "Player")
        {
            selectedInteractable = this;
            indicatorInstance.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {

        if (col.tag == "Player")
        {
            selectedInteractable = null;
            indicatorInstance.SetActive(false);
        }
    }

    public void OnTalk()
    {
        if (DialogueHandler.IS_RUNNING)
            return;
        TextAsset yes;
        if (onTalkDialogue == null)
            yes = Resources.Load<TextAsset>("Dialogue/DefaultTalk") as TextAsset;
        else
            yes = onTalkDialogue;

        FindObjectOfType<DialogueHandler>().StartEvent(yes);
    }

    public void OnLookAt()
    {
        if (DialogueHandler.IS_RUNNING)
            return;
        TextAsset yes;
        if (onLookAtDialogue == null)
            yes = Resources.Load<TextAsset>("Dialogue/DefaultLook") as TextAsset;
        else
            yes = onLookAtDialogue;

        FindObjectOfType<DialogueHandler>().StartEvent(yes);
    }

    public void OnTouch()
    {
        if (DialogueHandler.IS_RUNNING)
            return;
        TextAsset yes;
        if (onTouchDialogue == null)
            yes = Resources.Load<TextAsset>("Dialogue/DefaultTouch") as TextAsset;
        else
            yes = onTouchDialogue;

        FindObjectOfType<DialogueHandler>().StartEvent(yes);
    }

    public void OnUseItem(int id)
    {
        if (DialogueHandler.IS_RUNNING)
            return;

        DialogueHandler.variables.Remove("%itemID");
        DialogueHandler.variables.Add("%itemID", id);

        TextAsset yes;
        if (onItemDialogue == null)
            yes = Resources.Load<TextAsset>("Dialogue/DefaultItem") as TextAsset;
        else
            yes = onItemDialogue;

        FindObjectOfType<DialogueHandler>().StartEvent(yes);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
