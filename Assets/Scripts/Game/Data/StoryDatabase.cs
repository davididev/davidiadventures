﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryDatabase 
{
    public static StoryDatabase instance = new StoryDatabase();

    public class SingleStory
    {
        private string storyName, storyDescription, startingScene, endingDialogue;

        private Vector2 startingPosition;

        /// <summary>
        /// Set the data that shows up on the title
        /// </summary>
        /// <param name="name">NAme of adventure</param>
        /// <param name="description">Description adventure</param>
        public void SetTitleData(string name, string description)
        {
            storyName = name;
            storyDescription = description;
        }

        public void SetEndingDialogue(string s)
        {
            endingDialogue = s;
        }

        /// <summary>
        /// Set the data that starts out the game
        /// </summary>
        /// <param name="scene">Scene to start in</param>
        /// <param name="startingPos">Position to start</param>
        public void SetStartData(string scene, Vector2 startingPos)
        {
            startingScene = scene;
            startingPosition = startingPos;
        }


        public void PrepareLoadingScreen()
        {
            LoadingUI.levelToLoad = startingScene;
            LoadingUI.positionToGoTo = startingPosition;
        }

        /// <summary>
        /// Return the title and description.
        /// </summary>
        /// <returns></returns>
        public string StartTile()
        {
            return "<color=#4848FF>" + storyName + "</color>\n" + storyDescription;
        }

        /// <summary>
        /// Simply return the story title
        /// </summary>
        /// <returns></returns>
        public string GetStory()
        {
            return storyName;
        }

        /// <summary>
        /// When you complete the mission, this is the dialogue that appears.
        /// </summary>
        /// <returns></returns>
        public string GetEndingDialogue()
        {
            return endingDialogue;
        }
    }

    private SingleStory[] stories;


    /// <summary>
    /// Get all the quest summaries on the title screen.
    /// </summary>
    /// <returns></returns>
    public string[] GetTileData()
    {
        string[] s = new string[stories.Length];
        for(int i = 0; i < stories.Length; i++)
        {
            s[i] = stories[i].StartTile();
        }

        return s;
    }

    public string GetStoryName(int worldID)
    {
        return stories[worldID].GetStory();
    }

    public string GetEnding()
    {
        int worldID = SaveData.instance.worldID;
        return stories[worldID].GetEndingDialogue();
    }

    /// <summary>
    /// Prepare the Loading UI scene for the main quest.
    /// </summary>
    /// <param name="storyID">Which story ID</param>
    public void PrepareLoading(int storyID)
    {
        stories[storyID].PrepareLoadingScreen();
    }

    public StoryDatabase()
    {
        //stories = new SingleStory[1];
        stories = new SingleStory[2];

        //Tutorial
        stories[0] = new SingleStory();
        stories[0].SetTitleData("Tutorial", "This game isn't traditional; take this 2 minute tutorial to learn how to play.");
        stories[0].SetStartData("Tutorial", new Vector2(0f, 0f));

        //Marz 2019
        stories[1] = new SingleStory();
        stories[1].SetTitleData("Marz is cooped up!", "Marz is stuck inside Crazy Ville.  She has itchy feet and wishes to get out.");
        stories[1].SetStartData("CrazyVille", new Vector2(0f, 0f));
        stories[1].SetEndingDialogue("So after a hectic trading war, Marz finally set off on her new adventure.*Where will she go?  What will she do?*Well...she's the adventerous type.  She'll figure things out.");
        //Tutorial
        
    }
}
