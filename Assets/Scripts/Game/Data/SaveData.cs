﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SaveData 
{
    public static SaveData instance = new SaveData();
    public static int SaveFileID = 0;

    public int worldID = 0;  //World we are currently testing
    public Vector2 currentPosition = Vector2.zero;
    public string sceneName = "";
    public int[] itemsOwned = { 0, 0, 0, 0, 0, 0 };
    public int[] worldVars = { 0, 0, 0, 0 };
    
    /// <summary>
    /// Create a fresh file
    /// </summary>
    /// <param name="w">Story ID</param>
    /// <param name="startingScene">Unity scene the story starts in</param>
    public void FreshFile(int w, string startingScene)
    {
        worldID = w;
        sceneName = startingScene;

        currentPosition = Vector2.zero;
        for(int i = 0; i < itemsOwned.Length; i++)
        {
            itemsOwned[i] = 0;
        }
        for(int i = 0; i < worldVars.Length; i++)
        {
            worldVars[i] = 0;
        }
    }

    public SaveData()
    {
        //FreshFile(0, "CrazyVille");  //Default test scene
    }

    public void SaveFile()
    {
        sceneName = SceneManager.GetActiveScene().name;
        PlayerPrefs.SetInt("File" + SaveFileID + "WorldID", worldID);
        PlayerPrefs.SetString("File" + SaveFileID + "Position", currentPosition.x + "," + currentPosition.y);
        PlayerPrefs.SetString("File" + SaveFileID + "Scene", sceneName);
        PlayerPrefs.SetString("File" + SaveFileID + "Items", ArrayListToString(itemsOwned, itemsOwned.Length));
        PlayerPrefs.SetString("File" + SaveFileID + "Vars", ArrayListToString(worldVars, worldVars.Length));

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Load the file
    /// </summary>
    /// <returns>true if loaded, false if doesn't exit</returns>
    public bool LoadFile()
    {
        if (PlayerPrefs.HasKey("File" + SaveFileID + "WorldID") == false)
            return false;
        worldID = PlayerPrefs.GetInt("File" + SaveFileID + "WorldID");
        string[] c = PlayerPrefs.GetString("File" + SaveFileID + "Position").Split(',');
        currentPosition.Set(float.Parse(c[0]), float.Parse(c[1]));

        sceneName = PlayerPrefs.GetString("File" + SaveFileID + "Scene");

        int[] temp1 = StringToArrayList(PlayerPrefs.GetString("File" + SaveFileID + "Items"), itemsOwned.Length);
        for (int i = 0; i < temp1.Length; i++)
        {
            itemsOwned[i] = temp1[i];
        }
        int[] temp2 = StringToArrayList(PlayerPrefs.GetString("File" + SaveFileID + "Vars"), worldVars.Length);
        for (int i = 0; i < temp2.Length; i++)
        {
            worldVars[i] = temp2[i];
        }

        return true;
    }

    public void DeleteFile()
    {
        PlayerPrefs.DeleteKey("File" + SaveFileID + "WorldID");
        PlayerPrefs.DeleteKey("File" + SaveFileID + "Position");
        PlayerPrefs.DeleteKey("File" + SaveFileID + "Scene");
        PlayerPrefs.DeleteKey("File" + SaveFileID + "Items");
        PlayerPrefs.DeleteKey("File" + SaveFileID + "Vars");

        PlayerPrefs.Save();
    }

    /// <summary>
    /// Turn an array of ints to a string
    /// </summary>
    /// <param name="array">The data of the array</param>
    /// <param name="size">If the size of the list has to change, this is mandatory</param>
    /// <returns></returns>
    private string ArrayListToString(int[] array, int size)
    {
        string ret = "";
        for(int i = 0; i < size; i++)
        {
            if (i >= array.Length)
                ret = ret + "0 ";
            else
                ret = ret + array[i] + " ";
        }
        return ret;
    }

    /// <summary>
    /// Turn a string (the data) into an int array
    /// </summary>
    /// <param name="array">the data string that represents the int array</param>
    /// <param name="size">The size of the array.  If the size changes, this is important.</param>
    /// <returns></returns>
    private int[] StringToArrayList(string array, int size)
    {
        int[] list = new int[size];
        string[] numbers = array.Split(' ');

        for(int i = 0; i < size; i++)
        {
            if (i >= numbers.Length)
                list[i] = 0;
            else
                list[i] = int.Parse(numbers[i]);
        }
        return list;
    }

}
