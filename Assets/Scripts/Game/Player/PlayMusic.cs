﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayMusic : MonoBehaviour
{
    private const float FADE_TIME = 1f;
    public AudioClip currentSceneMusic;

    private static PlayMusic instance;

    private AudioSource clip1, clip2;
    private bool currentlyPlayingClip1 = true;
    private static string lastAudioName;

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
            clip1 = gameObject.AddComponent<AudioSource>();
            clip2 = gameObject.AddComponent<AudioSource>();
            clip1.loop = true;
            clip2.loop = true;
            Play(currentSceneMusic);
            Object.DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            instance.Play(currentSceneMusic);
            Destroy(this.gameObject);
        }
    }

    public void DestroyMe()
    {
        lastAudioName = "LOLWUT";
        Destroy(instance.gameObject);
        instance = null;
    }

    void SetClip1Vol(float v)
    {
        clip1.volume = v;
    }

    void SetClip2Vol(float v)
    {
        clip2.volume = v;
    }

    public void Play(AudioClip clip)
    {
        if (lastAudioName == clip.name)  //Same clip- don't restart,
            return;
        lastAudioName = clip.name;
        if (currentlyPlayingClip1)
        {
            clip2.clip = clip;
            iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "onupdate", "SetClip2Vol", "time", FADE_TIME));
            iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "onupdate", "SetClip1Vol", "time", FADE_TIME));
            clip2.Play();
        }
        else
        {
            clip1.clip = clip;
            iTween.ValueTo(gameObject, iTween.Hash("from", 1f, "to", 0f, "onupdate", "SetClip2Vol", "time", FADE_TIME));
            iTween.ValueTo(gameObject, iTween.Hash("from", 0f, "to", 1f, "onupdate", "SetClip1Vol", "time", FADE_TIME));
            clip1.Play();
        }
    }
    // Update is called once per frame
    void Update()
    {
        transform.position = Camera.main.transform.position;
    }
}
