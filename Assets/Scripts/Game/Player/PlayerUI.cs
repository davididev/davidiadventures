﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] private Image[] itemImages;
    [SerializeField] private TMPro.TextMeshProUGUI saveTextObj;

    public GameObject interactButtons;
    void Start()
    {
        
    }
    private bool lastActive = false;
    // Update is called once per frame
    void Update()
    {
        bool currentActive = Interactable.selectedInteractable != null;
        if (lastActive != currentActive)
        {
            interactButtons.SetActive(currentActive);
            lastActive = currentActive;

            if(currentActive == true)
            {
                RefreshItems();
            }
        }
    }

    /// <summary>
    /// Redraw the Items UI
    /// </summary>
    public void RefreshItems()
    {
        for (int i = 0; i < SaveData.instance.itemsOwned.Length; i++)
        {
            int quest = SaveData.instance.worldID;
            int itemID = SaveData.instance.itemsOwned[i];
            if (itemID == 0)
                itemImages[i].gameObject.SetActive(false);
            else
            {
                itemImages[i].gameObject.SetActive(true);
                itemImages[i].sprite = Resources.Load<Sprite>("ItemIcons/" + quest + "/" + itemID) as Sprite;
            }

        }
    }

    public void Talk()
    {
        Debug.Log("An hors");
        Interactable.selectedInteractable.OnTalk();
    }

    public void LookAt()
    {
        Interactable.selectedInteractable.OnLookAt();
    }

    public void OnTouch()
    {
        Interactable.selectedInteractable.OnTouch();
    }

    public void UseItem(int id)
    {
        int itemID = SaveData.instance.itemsOwned[id];
        if(itemID != 0)
            Interactable.selectedInteractable.OnUseItem(itemID);


    }

    public void SaveGame()
    {
        GameObject play = GameObject.FindGameObjectWithTag("Player");
        if (play != null)
        {
            Vector2 v = new Vector2(play.transform.position.x, play.transform.position.y);
            SaveData.instance.currentPosition = v;
        }
        SaveData.instance.SaveFile();
        if (saveCor != null)
            StopCoroutine(saveCor);
        saveCor = StartCoroutine(saveText());
    }

    private Coroutine saveCor;

    IEnumerator saveText()
    {
        saveTextObj.text = "Game Saved!";
        yield return new WaitForSeconds(1f);
        saveTextObj.text = "";
    }

    public GameObject quitConfirmPanel;

    public void PressQuitButton()
    {
        quitConfirmPanel.SetActive(true);
    }

    public void CancelQuit()
    {
        quitConfirmPanel.SetActive(false);
    }

    public void GoToTitle()
    {
        SceneManager.LoadScene("Title");
    }
}
