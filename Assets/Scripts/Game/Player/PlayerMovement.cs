﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private CharController chara;
    // Start is called before the first frame update
    void Start()
    {
        Vector2 v = LoadingUI.positionToGoTo;
        transform.position = new Vector3(v.x, v.y, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        if(chara == null)
            chara = GetComponent<CharController>();
        if (DialogueHandler.IS_RUNNING == false)
        {
            if (Input.GetMouseButton(0))
            {
                Vector3 screenPos = Input.mousePosition;
                screenPos.z = -15f;
                float cutoff = Screen.height * 3f / 8f;
                if (screenPos.y < Screen.height - cutoff)
                {
                    Ray r = Camera.main.ScreenPointToRay(screenPos);
                    Vector3 v = r.GetPoint(15f);
                    chara.targetPosition = new Vector2(v.x, v.y);
                }
            }
            else
                chara.targetPosition = Vector2.zero;
        }
    }
}
