﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialCanvas : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI text;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Tutorial());
    }

    IEnumerator Tutorial()
    {
        if (Application.isMobilePlatform)
            text.text = "Press and hold to walk.  Go over to the couch";
        else
            text.text = "Hold down the left mouse button to walk.  Go over to the couch";

        bool success = false;
        while (success == false)
        {
            if (Interactable.selectedInteractable != null)
            {
                if (Interactable.selectedInteractable.gameObject.name == "Couch Interact")
                    success = true;
            }
            yield return new WaitForEndOfFrame();
        }
        text.text = "Try the buttons on the top of the screen.";
        
        while (SaveData.instance.itemsOwned[0] == 0)
        {
            yield return new WaitForEndOfFrame();
        }

         text.text = "Unlock the door.";

         Transform player = GameObject.FindGameObjectWithTag("Player").transform;

        while (player.position.y < 9f)
        {
            yield return new WaitForEndOfFrame();
        }
        
        text.text = "Okay, we're done!";
        yield return new WaitForSeconds(5f);
        
        SceneManager.LoadScene("Title");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
