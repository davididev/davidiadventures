﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Rigidbody2D))]
public class CharController : MonoBehaviour
{
    private Animator anim;
    private Rigidbody2D rigid;
    private SpriteRenderer spriteRend;
    public float moveSpeed = 4f;

    public Vector2 targetPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (anim == null)
            anim = GetComponent<Animator>();
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
        if (spriteRend == null)
            spriteRend = GetComponent<SpriteRenderer>();

        float speed = rigid.velocity.sqrMagnitude;
        Vector2 direction = rigid.velocity.normalized;

        anim.SetFloat("Time Scale", speed / 4f);  //4 units per second is normal speed

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        
        if (angle < 0f)
            angle += 360f;
        if (angle > 360f)
            angle -= 360f;
        if (speed != 0f)
        {
            Debug.Log("Angle: " + angle);
        }
        if (angle > 0f && angle <= 45f)
            anim.SetInteger("Walk Dir", 2);
        if (angle > 45f && angle <= 135f)
            anim.SetInteger("Walk Dir", 1);
        if (angle > 135f && angle <= 225f)
            anim.SetInteger("Walk Dir", 4);
        if (angle > 225f)
            anim.SetInteger("Walk Dir", 3);

        int sortingOrder = -1 * Mathf.FloorToInt((transform.position.y) * 4f);
        spriteRend.sortingOrder = sortingOrder;
    }

    private void FixedUpdate()
    {
        if (rigid == null)
            rigid = GetComponent<Rigidbody2D>();
        
        if(targetPosition ==Vector2.zero)
        {
            rigid.velocity = Vector2.zero;
        }
        else
        {
            if(Vector2.Distance(targetPosition, rigid.position) < 0.35f)
            {
                rigid.velocity = Vector2.zero;
                return; 
            }
            Vector2 v = (targetPosition - rigid.position).normalized * moveSpeed;
            rigid.velocity = v;
        }
    }
}
