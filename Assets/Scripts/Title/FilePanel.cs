﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FilePanel : MonoBehaviour
{
    public FilePiece[] filePanels;
    // Start is called before the first frame update
    void OnEnable()
    {
        for(int i = 0; i < filePanels.Length; i++)
        {
            filePanels[i].Setup(i);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
