﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainPanel : MonoBehaviour
{
    public GameObject quitButton;

    // Start is called before the first frame update
    void OnEnable()
    {
        if (Application.isMobilePlatform == false)
            quitButton.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    
}
