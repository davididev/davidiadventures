﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreditsPanel : MonoBehaviour
{
    [SerializeField] private TextAsset creditsFile;
    [SerializeField] private TMPro.TextMeshProUGUI text;
    [SerializeField] private AnimationClip clipFade;

    private Animator textAnim;

    private Coroutine c1;

    // Start is called before the first frame update
    void OnEnable()
    {
        c1 = StartCoroutine(CreditsCoroutine());
    }

    private void OnDisable()
    {
        StopCoroutine(c1);
    }

    IEnumerator CreditsCoroutine()
    {
        float FADE_DIST = clipFade.length / 2f;
        textAnim = text.GetComponent<Animator>();
        string[] pieces = creditsFile.text.Split('*');
        while(gameObject)
        {
            for(int i = 0; i < pieces.Length; i++)
            {
                text.text = pieces[i];
                yield return new WaitForSeconds(FADE_DIST);
                yield return new WaitForSeconds(4f);
                textAnim.SetTrigger("Fade");
                yield return new WaitForSeconds(FADE_DIST);

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
