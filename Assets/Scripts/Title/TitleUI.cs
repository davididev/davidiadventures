﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TitleUI : MonoBehaviour
{
    [SerializeField] private GameObject mainPanel, filePanel, questSelectionPanel, creditsPanel;
    // Start is called before the first frame update
    void Start()
    {
        DialogueHandler.ClearVars();
        PlayMusic wut = FindObjectOfType<PlayMusic>();
        if (wut != null)
        {
            wut.DestroyMe();
        }


        if (Application.isMobilePlatform)
            Screen.orientation = ScreenOrientation.Portrait;
        SetPanelID(0);
    }

    public void SetPanelID(int id)
    {
        mainPanel.SetActive(id == 0);
        filePanel.SetActive(id == 1);
        questSelectionPanel.SetActive(id == 2);
        creditsPanel.SetActive(id == 3);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
