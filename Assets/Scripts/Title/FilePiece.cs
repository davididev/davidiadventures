﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FilePiece : MonoBehaviour
{
    private int saveID = 0;
    public GameObject deleteButton;
    public TMPro.TextMeshProUGUI textData, deleteText;


    // Start is called before the first frame update
    void Start()
    {
        
    }
    int deleteConfirm = 0;
    public void Setup(int fileID)
    {
        deleteText.text = "Delete";
        deleteConfirm = 0;
        saveID = fileID;
        SaveData.SaveFileID = saveID;
        string str = "";
        if (SaveData.instance.LoadFile() == true)
        {
            int worldID = SaveData.instance.worldID;
            str = StoryDatabase.instance.GetStoryName(worldID);
            deleteButton.SetActive(true);
        }
        else
        {
            str = "New File";
            deleteButton.SetActive(false);
        }
        
        textData.text = "File " + (saveID+1) + ":\n" + str;
    }

    public void Play()
    {
        SaveData.SaveFileID = this.saveID;
        if(deleteButton.activeSelf == false)
        {
            //New file- go to quest selection
            FindObjectOfType<TitleUI>().SetPanelID(2);
            return;
        }
        SaveData.SaveFileID = saveID;
        SaveData.instance.LoadFile();

        LoadingUI.levelToLoad = SaveData.instance.sceneName;
        LoadingUI.positionToGoTo = SaveData.instance.currentPosition;

        SceneManager.LoadScene("Loading");
    }

    public void Delete()
    {
        if(deleteConfirm == 0)
        {
            deleteText.text = "You sure?";
            deleteConfirm = 1;
            return;
        }
        if(deleteConfirm == 1)
        {
            SaveData.SaveFileID = this.saveID;
            SaveData.instance.DeleteFile();

            Setup(saveID);
        }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
