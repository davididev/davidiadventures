﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingUI : MonoBehaviour
{
    public static string levelToLoad = "CrazyVille";
    public static Vector2 positionToGoTo = Vector2.zero;
    [SerializeField] private TMPro.TextMeshProUGUI loadingText;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(YesIWillMarryYou());
    }

    IEnumerator YesIWillMarryYou()
    {
        AsyncOperation lolwut = SceneManager.LoadSceneAsync(levelToLoad);
        while(lolwut.isDone == false)
        {
            int textID = Mathf.FloorToInt(lolwut.progress * 6f);
            if (textID == 0)
                loadingText.text = "Allocating oatmeal";
            if (textID == 1)
                loadingText.text = "Scattering oatmeal";
            if (textID == 2)
                loadingText.text = "Blessing oatmeal";
            if (textID == 3)
                loadingText.text = "Eating oatmeal";
            if (textID == 4)
                loadingText.text = "Mourning oatmeal";
            if (textID >= 5)
                loadingText.text = "Loading level";

            yield return new WaitForEndOfFrame();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
