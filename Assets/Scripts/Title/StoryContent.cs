﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;


public class StoryContent : MonoBehaviour
{
    private const float UI_HEIGHT = 175f;
    public Button goButtonOriginal;
    public TMPro.TextMeshProUGUI textOriginal;

    private Button[] buttons;
    private TMPro.TextMeshProUGUI[] texts;
    // Start is called before the first frame update
    void OnEnable()
    {
        string[] db = StoryDatabase.instance.GetTileData();
        if(buttons == null)
        {
            buttons = new Button[db.Length];
            texts = new TMPro.TextMeshProUGUI[db.Length];

            buttons[0] = goButtonOriginal;
            buttons[0].onClick.AddListener(() => LoadStoryID(0));
            texts[0] = textOriginal;
            texts[0].text = db[0];

            for (int i = 1; i < db.Length; i++)
            {
                GameObject gb = GameObject.Instantiate(goButtonOriginal.gameObject, goButtonOriginal.transform.parent);
                RectTransform rt1 = gb.GetComponent<RectTransform>();
                Vector2 v1 = rt1.anchoredPosition;
                v1.y -= UI_HEIGHT* i;
                rt1.anchoredPosition = v1;
                buttons[i] = gb.GetComponent<Button>();
                buttons[i].onClick.AddListener(() => LoadStoryID(1));
                

                GameObject gt = GameObject.Instantiate(textOriginal.gameObject, textOriginal.transform.parent);
                RectTransform rt2 = gt.GetComponent<RectTransform>();
                Vector2 v2 = rt2.anchoredPosition;
                v2.y -= UI_HEIGHT* i;
                rt2.anchoredPosition = v2;
                texts[i] = gt.GetComponent<TMPro.TextMeshProUGUI>();

                texts[i].text = db[i];
            }
        }
    }

    
    public void LoadStoryID(int id)
    {
        SaveData.instance.FreshFile(id, "");
        StoryDatabase.instance.PrepareLoading(id);  //This function is for new files.
        SceneManager.LoadScene("Loading");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
