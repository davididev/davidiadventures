﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class Actor : MonoBehaviour {

    public string actorName;
    public static Dictionary<string, Actor> actors = new Dictionary<string, Actor>();
    public float moveSpeed = 5f;
    public float MIN_DISTANCE = 2f;
    public float maxSpeed = 10f;
    public float rotateSpeed = 1800f;
    public bool ignoreY = true;
    public bool activeOnStart = true, applyGravity = false;
	[HideInInspector] public bool runningPath = false;
	
    private CharacterController rigid;

    private Queue<Vector3> path = new Queue<Vector3>();

    // Use this for initialization
    void Start () {
        actors.Add(actorName, this);
        rigid = GetComponent<CharacterController>();
        gameObject.SetActive(activeOnStart);
    }

    void OnDestroy()
    {
        actors.Remove(actorName);
    }
	
	// Update is called once per frame
	void Update () {
        if (applyGravity)
        {
            if (rigid.isGrounded)
                gravity = -.01f;

            gravity = gravity - 9.8f * Time.fixedDeltaTime;
            rigid.Move(Vector3.up * gravity);
        }
    }

    public void MoveActor(Vector3[] points)
    {

        foreach(Vector3 p in points)
        {
            path.Enqueue(p);
        }
        runningPath = true;
        StartCoroutine(RunPath());
    }
    

    IEnumerator RunPath()
    {
		runningPath = true;
        Debug.Log("path size: " + path.Count);
        while(path.Count > 0)
        {
            Vector3 pointToLookAt = path.Peek();

            float distance = 500f;
            while (distance > MIN_DISTANCE)
            {
                Vector3 center = rigid.center + transform.position;
                Quaternion q1 = Quaternion.LookRotation(pointToLookAt - center);
                //float targetRot = Mathf.Atan2(pointToLookAt.x - center.x, pointToLookAt.z - center.z) * Mathf.Rad2Deg;
                float targetRot = q1.eulerAngles.y;
                if (ignoreY)
                    distance = Vector2.Distance(new Vector2(center.x, center.z), new Vector2(pointToLookAt.x, pointToLookAt.z));
                else
                    distance = Vector3.Distance(center, pointToLookAt);



                Vector3 rot = transform.eulerAngles;
                
                float moveAngle = Mathf.MoveTowardsAngle(rot.y, targetRot, rotateSpeed * Time.deltaTime);
                Debug.Log("Current: " + rot.y + "; Target: " + targetRot + "; moveAngle: " + moveAngle + "; distance: " + distance);
                rot.y = moveAngle;
                transform.eulerAngles = rot;

                float d = moveSpeed * Time.fixedDeltaTime;
                if (d > distance)
                    d = distance;

                Debug.Log(d);
                rigid.Move((transform.forward * d));
                //rigid.AddRelativeForce(Vector3.forward * moveSpeed, ForceMode.Acceleration);

                if (!ignoreY && Mathf.Abs(center.y - pointToLookAt.y) > MIN_DISTANCE)
                    //rigid.AddForce(transform.up * moveSpeed * ((rigid.position.y < pointToLookAt.y ) ? 1f : -1f), ForceMode.Acceleration);
                    rigid.Move((transform.up * moveSpeed * Time.fixedDeltaTime));
                yield return new WaitForFixedUpdate();
            }

            Vector3 no = path.Dequeue();
        }
        Debug.Log("Path finished");

        runningPath = false;

        
    }

    float gravity = 0f;
}
