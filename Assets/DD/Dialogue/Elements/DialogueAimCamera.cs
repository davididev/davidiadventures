﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;


[System.Serializable]
public class DialogueAimCamera : DialogueItem {

    public Vector3 positionToMoveTo;
    public string actorToMoveTo;
    public string actorToLookAt;
    public Vector3 positionToLookAt;

    public bool moveToActor;
    public bool lookAtActor;
    public float transitionTime;
    public iTween.EaseType ease;
    

    public override IEnumerator Run()
    {
        GameObject cam = Camera.main.gameObject;

        Vector3 pos = positionToMoveTo;
        if(moveToActor)
        {
            Actor a;
            if(Actor.actors.TryGetValue(actorToMoveTo, out a))
            {
                pos = a.transform.position;
            }
        }

        Vector3 lookAt = positionToLookAt;
        if(lookAtActor)
        {
            Actor a;
            if(Actor.actors.TryGetValue(actorToLookAt, out a))
            {
                lookAt  = a.transform.position;
            }
        }

        Vector3 angles = Quaternion.LookRotation(lookAt - pos).eulerAngles;

        if (transitionTime > 0f)
        {
            iTween.MoveTo(cam, iTween.Hash("position", pos, "time", transitionTime, "easetype", ease));
            iTween.RotateTo(cam, iTween.Hash("rotation", angles, "time", transitionTime, "easetype", ease));
        }
        else
        {
            cam.transform.position = pos;
            cam.transform.eulerAngles = angles;
        }

        yield return new WaitForSeconds(transitionTime);
        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);

        breakpoint = GUILayout.Toggle(breakpoint, "Is a breakpoint?");

        transitionTime = this.FloatField("Transition time", transitionTime);

        ease = (iTween.EaseType) EnumPopup("Ease Type: ", ease);

        moveToActor = GUILayout.Toggle(moveToActor, "Move Towards Actor: ");
        if(moveToActor)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Actor to Move To: ");
            actorToMoveTo = GUILayout.TextField(actorToMoveTo);
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Position to move to: ");
            float x = positionToMoveTo.x;
            float y = positionToMoveTo.y;
            float z = positionToMoveTo.z;
            x = this.FloatField("x", x);
            y = this.FloatField("y", y);
            z = this.FloatField("z", z);
            positionToMoveTo.Set(x, y, z);
            GUILayout.EndHorizontal();
        }

        lookAtActor = GUILayout.Toggle(lookAtActor, "Look Towards Actor: ");
        if (lookAtActor)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Actor to Look To: ");
            actorToLookAt = GUILayout.TextField(actorToLookAt);
            GUILayout.EndHorizontal();
        }
        else
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Position to look to: ");
            float x = positionToLookAt.x;
            float y = positionToLookAt.y;
            float z = positionToLookAt.z;
            x = this.FloatField("x", x);
            y = this.FloatField("y", y);
            z = this.FloatField("z", z);
            positionToLookAt.Set(x, y, z);
            GUILayout.EndHorizontal();
        }
    }
}
