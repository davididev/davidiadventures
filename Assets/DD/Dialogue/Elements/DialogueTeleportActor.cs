﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTeleportActor : DialogueItem {

    public string actorToMove;
    public Vector3 positionToMoveTo;
    public string actorToMoveTo;
    public string sceneToMoveTo;
    public bool moveViaActor;


    public DialogueTeleportActor()
    {

    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        if (sceneToMoveTo == "")  //Teleporting in scene
        {
            Actor a1;
            if (Actor.actors.TryGetValue(actorToMove, out a1))
            {
                Vector3 newPos = positionToMoveTo;
                if (moveViaActor)
                {
                    Actor a2;
                    if (Actor.actors.TryGetValue(actorToMoveTo, out a2))
                    {
                        newPos = a2.transform.position;
                    }
                    else
                        Debug.Log("<color=red>Actor to move to (" + actorToMoveTo + ") not found.  Sorry.</color>");
                }


                CharacterController cc = a1.GetComponent<CharacterController>();
                if (cc != null)
                    cc.enabled = false;

                a1.transform.position = newPos;

                if (cc != null)
                    cc.enabled = true;
            }
            else
            {
                Debug.Log("<color=red>Actor to move (" + actorToMove + ") not found.  Sorry.</color>");
            }
        }
  
        if(sceneToMoveTo != "")  //Teleporting outside of scene (assume hero)
        {
            LoadingUI.levelToLoad = sceneToMoveTo;
            LoadingUI.positionToGoTo = positionToMoveTo;
            UnityEngine.SceneManagement.SceneManager.LoadScene("Loading");
        }

        completed = true;
    }



    public override void DrawEditor(Rect verticalGroup)
    {
        base.DrawEditor(verticalGroup);

        GUILayout.BeginHorizontal();
        GUILayout.Label("Actor to move");
        actorToMove = GUILayout.TextField(actorToMove);
        GUILayout.EndVertical();

        moveViaActor = GUILayout.Toggle(moveViaActor, "Teleport to empty actor");
        if(moveViaActor == true)
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Empty Actor to move to:");
            actorToMoveTo = GUILayout.TextField(actorToMoveTo);
            GUILayout.EndVertical();
        }

        if(moveViaActor == false)
        {
            GUILayout.Label("Position to move to:");
            GUILayout.BeginHorizontal();
            Vector3 v = positionToMoveTo;
            v.x = FloatField("X: ", v.x);
            v.y = FloatField("Y: ", v.y);
            v.z = FloatField("Z: ", v.z);
            positionToMoveTo = v;

            GUILayout.EndVertical();
        }

        if (GUILayout.Button("Clear Scene"))
            sceneToMoveTo = "";
        GUILayout.Label("Scene to move to (leave blank if nowhere)");
        sceneToMoveTo = GUILayout.TextField(sceneToMoveTo);
    }

}
