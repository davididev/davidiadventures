﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DialogueMoveActor : DialogueItem
{

    public string actorName = "";

    public List<Vector3> points = new List<Vector3>();
    public List<string> actorPoints = new List<string>();
    public bool useActorPoints = false;

    public override IEnumerator Run()
    {
        yield return base.Run();
        Actor myActor;
        if(Actor.actors.TryGetValue(actorName, out myActor))
        {
            if(useActorPoints)
            {
                //Use actor names as a path
                for (int i = 0; i < actorPoints.Count; i++)
                {
                    Actor pt;
                    if (Actor.actors.TryGetValue(actorPoints[i], out pt))
                    {
                        points.Add(pt.transform.position);
                    }
                    else
                    {
                        Debug.Log("<color=red>Could not get point " + i + " by name of " + pt.actorName + ".</color>");
                    }

                }
                
            }
            myActor.MoveActor(points.ToArray());
            while (myActor.runningPath) { yield return new WaitForEndOfFrame(); }

        }
        else
        {
            Debug.Log("<color=red>Could not find actor by the ID of " + myActor.actorName + "</color>");
        }

        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Move this fellar");
        GUILayout.Label("Please note that WaitForCompletion should be added ");
        GUILayout.Label("if you want to wait until after the movement ");
        GUILayout.Label("to play the next event.");
        actorName = GUILayout.TextField(actorName);
        useActorPoints = GUILayout.Toggle(useActorPoints, "Use empty actors as path: ");

        if (GUILayout.Button("+", GUILayout.Width(30f)))
        {
            if (useActorPoints)
                actorPoints.Add("");
            else
                points.Add(new Vector3());
        }

        GUILayout.Space(50f);

        if (useActorPoints)
        {
            int i = 0;
            List<string>.Enumerator e1 = actorPoints.GetEnumerator();
            while(e1.MoveNext())
            {
                string current = e1.Current;
                GUILayout.BeginHorizontal();
                GUILayout.Label("pt " + i + ":");
                current = GUILayout.TextField(current);
                GUILayout.EndHorizontal();
               actorPoints[i] = current;
                i++;
            }
        }
        else
        {
            int i = 0;
            List<Vector3>.Enumerator e1 = points.GetEnumerator();
            while (e1.MoveNext())
            {
                Vector3 current = e1.Current;
                GUILayout.BeginHorizontal();
                current.x = this.FloatField("x", e1.Current.x);
                current.y = this.FloatField("y", e1.Current.y);
                current.z = this.FloatField("z", e1.Current.z);
                GUILayout.EndHorizontal();
                points[i] = current;
                i++;
            }
        }

        completed = true;
    }

    public override string ToString()
    {
        return "Move actor";
    }
}
