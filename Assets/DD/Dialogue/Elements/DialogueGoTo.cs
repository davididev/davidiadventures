﻿using UnityEngine;
using System.Collections;

public class DialogueGoTo : DialogueItem
{
    public int lineToGoTo = 0;
    public DialogueGoTo()
    {
        breakpoint = true;
    }
    public override IEnumerator Run()
    {
        yield return base.Run();
        handle.GotoLine(lineToGoTo);
        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        lineToGoTo = this.IntField("Line to go to: ", lineToGoTo);
    }

    public override string ToString()
    {
        return "Goto";
    }
}
