﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueAddOrRemoveItem : DialogueItem
{
    public bool addItem = true;  //set to false if you remove item
    public int itemID = 1;
    public override IEnumerator Run()
    {
        yield return base.Run();
        if(addItem == true)
        {
            //Add the item ID
            for(int i = 0; i < SaveData.instance.itemsOwned.Length; i++)
            {
                if(SaveData.instance.itemsOwned[i] == 0)
                {
                    SaveData.instance.itemsOwned[i] = itemID;
                    break;
                }
            }
        }
        else
        {
            //Remove the item ID
            for (int i = 0; i < SaveData.instance.itemsOwned.Length; i++)
            {
                if(SaveData.instance.itemsOwned[i] == itemID)
                {
                    SaveData.instance.itemsOwned[i] = 0;
                    break;
                }
            }
        }

        yield return null;

        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        addItem = GUILayout.Toggle(addItem, "Add item? (uncheck if remove)");
        itemID = this.IntField("Item ID", itemID);

    }
}
