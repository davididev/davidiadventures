﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialoguePlaySound : DialogueItem
{
    public string soundToPlay;
    public bool waitUntilDone = false;

    public DialoguePlaySound()
    {
        breakpoint = true;
    }

    public override IEnumerator Run()
    {
        yield return base.Run();
        AudioClip clip = Resources.Load<AudioClip>("Audio/" + soundToPlay) as AudioClip;
        float length = clip.length;

        AudioSource.PlayClipAtPoint(clip, Camera.main.transform.position);
        yield return new WaitForEndOfFrame();
        if(waitUntilDone)
        {
            yield return new WaitForSeconds(length);
        }

        completed = true;

        
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Sound to play: ");
        soundToPlay = GUILayout.TextField(soundToPlay);
        GUILayout.EndHorizontal();

        waitUntilDone = GUILayout.Toggle(waitUntilDone, "Wait until done?");
    }
}
