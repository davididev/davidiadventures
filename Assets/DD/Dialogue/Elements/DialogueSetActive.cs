﻿using System.Collections;
using UnityEngine;

public class DialogueSetActive : DialogueItem
{
    public string actorName = "Actor";
    public bool activeSelf = true;
    public override IEnumerator Run()
    {
        yield return base.Run();
        Actor act;
        if(Actor.actors.TryGetValue(actorName, out act))
        {
            act.gameObject.SetActive(activeSelf);
        }
        else
        {
            Debug.LogError("<color=red>Cannot find actor " + actorName + " to set active (" + activeSelf + ").</color>");
        }
        completed = true;
    }

    public override void DrawEditor(Rect verticalGroup)
    {
        GUILayout.Label("Actor Name");
        actorName = GUILayout.TextField(actorName);
        activeSelf = GUILayout.Toggle(activeSelf, "Active self");
    }

    public override string ToString()
    {
        return "Set active";
    }
}