﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SortingLayerSet))]
public class SortingLayerSetEditor : Editor
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public override void OnInspectorGUI()
    {
        SortingLayerSet copy = (SortingLayerSet)target;
        GUILayout.Label("Set the sorting layer to the Y coordinates.");
        if(GUILayout.Button("Set Children") == true)
        {
            SpriteRenderer[] kidrens = copy.transform.GetComponentsInChildren<SpriteRenderer>();
            foreach(SpriteRenderer minor in kidrens)
            {
                int sortingOrder = -1 * Mathf.FloorToInt((minor.transform.position.y) * 4f);
                minor.sortingOrder = sortingOrder;
            }
        }
    }
}
